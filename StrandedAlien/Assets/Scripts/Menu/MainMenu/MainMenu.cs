﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    GameObject StartImage;
    [SerializeField]
    GameObject OptionsMenu;
    [SerializeField]
    float StartImageShowTime;
    private float time;
    private bool startGame;
    private bool activateScene;
    void Start() 
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        StartImage.SetActive(false);
        startGame = false;
        activateScene = false;
        time = 0;
    }

    void Update() 
    {
        if (startGame) 
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            time += Time.deltaTime;
            if (time >= StartImageShowTime) 
            {
                activateScene = true;
            }
        }
    }

    //Play Button
    public void PlayGame()
    {
        foreach (Transform child in transform) 
        {
            child.gameObject.SetActive(false);
        }
        OptionsMenu.SetActive(false);
        StartImage.SetActive(true);
        startGame = true;
        StartCoroutine(LoadScene());
    }

    //Quit Button
    public void QuitGame()
    {
        Application.Quit();
    }

    IEnumerator LoadScene() 
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("Planet", LoadSceneMode.Single);
        asyncOperation.allowSceneActivation = false;
        while (!asyncOperation.isDone) 
        {
            if (activateScene && asyncOperation.progress >= 0.9f) 
            {
                asyncOperation.allowSceneActivation = true;
            }
            yield return null;
        }
    }
}
