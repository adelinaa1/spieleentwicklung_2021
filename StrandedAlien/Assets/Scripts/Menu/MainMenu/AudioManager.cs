﻿using System;
using UnityEngine;
using System.Collections.Generic;

//was man einfügen muss wenn man einen Sound zb bei einer 
//Collision einfügen will, in deren Klasse muss dann:
//FindObjectOfType<AudioManager>().Play(" hier der Name der Audio ");

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager instance;
    private List<String> AudioQueue;
    private AudioSource CurrentAudio;
    private float AudioQueueDelay;
 
    void Awake()
    {
        //dann hier prüfen ob es grade 2 audiomanager sind
        //wenn ja einen löschen
        if(instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
            //damit auch kein code mehr aufgerufen wird
        }

        //damit es die Musik nich bei unterschiedlichen Szenen abkackt
        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;

            //audiomixer krampf
            s.source.outputAudioMixerGroup = s.mix;

            //audio wird leiser bei Pause
            //kömm mer etz auch erstmal weglassen so
            //is aber ganz cool
            //if(PauseMenu.GameIsPaused)
            //{
            //    s.source.pitch *= .5f;
            //}
        }
    }

    public AudioSource Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
           //für wenn man sich vertippt das kein null reference fehler kommt
           //das es nicht versucht einen sound abzuspielen den es nicht gibt
           if(s == null)
           {
             Debug.LogWarning("Du hast dich im Namen verschrieben!");
            Debug.LogWarning("Der angegebene Name ist: " + name);
             return null;
           }
           
           s.source.volume = 0.3f;
           s.source.Play();
        return s.source;
    }

    //hier im AudioPlayer zb einfach die Musik laufen lassen
    //easy peasy
    void Start()
    {
        AudioQueue = new List<string>();
        CurrentAudio = null;
        AudioQueueDelay = 0;
        Play("Background_Probe");
    }

    void Update() 
    {
        PlayAudioFromAudioQueue();
    }

    public void ExtraLaut(string name)
    {
           Sound s = Array.Find(sounds, sound => sound.name == name);
           //für wenn man sich vertippt das kein null reference fehler kommt
           //das es nicht versucht einen sound abzuspielen den es nicht gibt
           if(s == null)
           {
             Debug.LogWarning("Du hast dich im Namen verschrieben!");
             return;
           }
           
           s.source.volume = 0.5f;
           s.source.Play();
    }

        public void Leiser(string name)
    {
           Sound s = Array.Find(sounds, sound => sound.name == name);
           //für wenn man sich vertippt das kein null reference fehler kommt
           //das es nicht versucht einen sound abzuspielen den es nicht gibt
           if(s == null)
           {
             Debug.LogWarning("Du hast dich im Namen verschrieben!");
             return;
           }
           s.source.volume = 0.1f;
           s.source.Play();
    }

          public void Stop(string name)
    {
           Sound s = Array.Find(sounds, sound => sound.name == name);
           //für wenn man sich vertippt das kein null reference fehler kommt
           //das es nicht versucht einen sound abzuspielen den es nicht gibt
           if(s == null)
           {
             Debug.LogWarning("Du hast dich im Namen verschrieben!");
             return;
           }
           s.source.volume = 0f;
           s.source.Play();
    }

    public void Delay(string name)
    {
           Sound s = Array.Find(sounds, sound => sound.name == name);
           //für wenn man sich vertippt das kein null reference fehler kommt
           //das es nicht versucht einen sound abzuspielen den es nicht gibt
           if(s == null)
           {
             Debug.LogWarning("Du hast dich im Namen verschrieben!");
             return;
           }
           s.source.volume = 3f;
           s.source.PlayDelayed( 3 );
    }

    /**
     * die folgenden methoden bieten die möglichkeit audiodateien
     * in eine queue einzureihen. die audiodateien in der queue
     * werden nacheinander abgespielt. zwischen dem abspielen
     * ist eine pause von 2 sekunden
     * 
     * die methode nimmt wie play den name da datei entgegen und
     * reiht ih in die queue ein.
     * die methode zum abspielen nimmt das erste element und spielt
     * es ab.
     */
    public void AddAudioToAudioQueue(String name) 
    {
        AudioQueue.Add(name);
        Debug.Log("added to queue: "+name);
    }

    private void PlayAudioFromAudioQueue() 
    {
        if ((CurrentAudio != null && CurrentAudio.isPlaying) ||
            (AudioQueue != null && AudioQueue.Count == 0)) 
        {
            AudioQueueDelay = 0;
            return;
        }

        if (AudioQueueDelay >= 2) 
        {
            CurrentAudio = Play(AudioQueue[0]);
            AudioQueue.RemoveAt(0);
        }

        AudioQueueDelay += Time.deltaTime;
    }

    public bool IsAudioInQueueOrPlaying() 
    {
        if (AudioQueue != null) 
        {
            if (AudioQueue.Count > 0) 
            {
                return true;
            }
            if (CurrentAudio != null) 
            {
                return CurrentAudio.isPlaying;
            }
        }
        return false;
    }

    public void PauseCurrentAudio() 
    {
        if (CurrentAudio != null && CurrentAudio.isPlaying) 
        {
            CurrentAudio.Pause();
        }
    }

    public void ResumeCurrentAudio() 
    {
        if (CurrentAudio != null) 
        {
            CurrentAudio.UnPause();
        }
    }

}