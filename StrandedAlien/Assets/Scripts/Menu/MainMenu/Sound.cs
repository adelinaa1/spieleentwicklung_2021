﻿using UnityEngine.Audio;
using System;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public string name;

   public AudioClip clip;

    [Range(0f, 1f)]
   public float volume;
   [Range(.1f, 3f)]
   public float pitch;

   public bool loop;

   //hier des mit dem audiomixer probieren
    public AudioMixerGroup mix;
    

    [HideInInspector]
   public AudioSource source;
}
