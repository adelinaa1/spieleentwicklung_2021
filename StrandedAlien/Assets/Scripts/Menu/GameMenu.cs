﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject GameFlow;
    private GameFlowController GameFlowController;

    private GameObject Menu;

    private Transform BackButton;
    private Transform ExitButton;
    private Transform SliderLightIntensity;

    void Start()
    {
        GameFlowController = GameFlow.GetComponent<GameFlowController>();

        BackButton = transform.Find("Canvas/BackButton");
        ExitButton = transform.Find("Canvas/Content/ExitButton");
        SliderLightIntensity = transform.Find("Canvas/Content/GammaValue");
        Menu = transform.Find("Canvas").gameObject;

        Menu.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            if (Menu.activeSelf)
            {
                Menu.SetActive(false);
                GameFlowController.ResumeGame();
            }
            else 
            {
                Menu.SetActive(true);
                GameFlowController.PauseGame();
            }
        }
    }

    public void ExitButtonAction() 
    {
        Application.Quit();
    }

    public void BackButtonAction() 
    {
        Menu.SetActive(false);
        GameFlowController.ResumeGame();
    }

    /**
     * wird zu beginn einmal aufgerufen
     * wird benötigt, da das tablet erst angezeigt werden darf wenn alle start()-methoden fertig sind
     * da sonst probleme durch nicht aktive gameObjects auftreten können
     */
    /*private IEnumerator WaitUntilEndOfFrame()
    {
        yield return new WaitForEndOfFrame();
        DisplayTablet();
    }


    public void ConfirmButton()
    {
        QuestController.UpdateQuestState();
        SetContent();
    }

    public void ControlsButton() 
    {
        controls = !controls;
        SetContent();
    }

    private void SetContent()
    {
        //ProgressBar();
        Controls();
        FillTextArea();
    }

    private  void DisplayTablet()
    {
        /*GameFlowController.PauseGame();
        Tablet.SetActive(true);
        controls = false;
        SetContent();*/
    /*}

    /*public void DisplayTabletDependingOnQuestState() 
    {
        EnumQuest state = QuestController.GetState();
        if (state.Equals(EnumQuest.Holes) || state.Equals(EnumQuest.Leg) || state.Equals(EnumQuest.Antenna) 
            || state.Equals(EnumQuest.PowerCell) || state.Equals(EnumQuest.End))
        {
            DisplayTablet();
        }
    }

    private void HideTablet()
    {
        Tablet.SetActive(false);
        GameFlowController.ResumeGame();
    }

    private void Controls() 
    {
        if (controls)
        {
            ButtonControls.GetComponentInChildren<Text>().text = CONTROLS_BACK_TEXT;
            ButtonConfirm.gameObject.SetActive(false);
            SliderLightIntensity.gameObject.SetActive(true);
        }
        else 
        {
            ButtonControls.GetComponentInChildren<Text>().text = CONTROLS_MAIN_TEXT;
           
            SliderLightIntensity.gameObject.SetActive(false);
        }
    }

    private void ProgressBar() 
    {
        /*int progress = QuestController.GetRepairQuestProgress();
        if (progress > 0) 
        {
            ProgressBar1.GetComponent<Image>().color = HIGHLIGHT_COLOR;
        }
        if (progress > 1) 
        {
            ProgressBar2.GetComponent<Image>().color = HIGHLIGHT_COLOR;
        }
        if (progress > 2)
        {
            ProgressBar3.GetComponent<Image>().color = HIGHLIGHT_COLOR;
        }
        if (progress > 3)
        {
            ProgressBar4.GetComponent<Image>().color = HIGHLIGHT_COLOR;
        }*/
    /*}

    /*private void FillTextArea() 
    {
        //alle textelemente außer "MainText" löschen
        foreach (Transform child in TextArea)
        {
            if (!child.gameObject.name.Equals("MainText") && !child.gameObject.name.Equals("TaskText"))
            {
                Destroy(child.gameObject);
            }
        }

        //controls
        if (controls)
        {
            MainText.GetComponent<Text>().text = TextsV2.Controls;
            return;
        }

        Dictionary<string, string> texts = QuestController.GetQuestText();
        string mainText = "";
        string repairText = "";

        //fehlerfall
        if (texts == null)
        {
            Debug.LogError("Error in " + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name
                        + " in method " + System.Reflection.MethodBase.GetCurrentMethod().Name);
            mainText = "ERROR" + Environment.NewLine + "texts dictionary is null";
        }

        if (texts.ContainsKey(MAIN_TEXT)) 
        {
            mainText = texts[MAIN_TEXT];
            texts.Remove(MAIN_TEXT);
        }
        if (texts.ContainsKey(REPAIR_TEXT)) 
        {
            repairText = texts[REPAIR_TEXT];
            texts.Remove(REPAIR_TEXT);
        }

        if (texts.Count > 0) 
        {
            foreach (string key in texts.Keys) 
            {
                Transform t = GameObject.Instantiate(TaskText, TextArea);
                t.gameObject.SetActive(true);
                t.GetComponent<Text>().text = texts[key];
                if (QuestController.TabletGreyText(key)) 
                {
                    t.GetComponent<Text>().color = Color.grey;
                }
            }
        }

        if (!repairText.Equals("")) 
        {
            Transform t = GameObject.Instantiate(TaskText, TextArea);
            t.gameObject.SetActive(true);
            t.GetComponent<Text>().text = repairText;
        }

        MainText.GetComponent<Text>().text = mainText;
    }*/
}
