﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class SceneChange : MonoBehaviour
{

    public int scene;

    
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
        {
        if( scene == 1){

            SceneManager.LoadScene(1);
        }
    
        if( scene == 2){
            SceneManager.LoadScene(2);
        }
    }
}
