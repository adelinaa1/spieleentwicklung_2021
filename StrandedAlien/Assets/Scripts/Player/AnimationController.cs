﻿using UnityEngine;

/**
 * Springen: Leertaste
 * Interaktion: F
 * Klettern: C
 * Winken: G
 * Tanzen: J
 * Laufen: W,A,S,D
 * Rennen: Shift + W,A,S,D
 */
public class AnimationController : MonoBehaviour
{
    public Animator Animator;
    public bool InAnimation { private set; get; }

    void Start()
    {
        InAnimation = false;
    }

    void Update()
    {
        if (Animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
        {
            AnimatorClipInfo[] animatorInfo = Animator.GetCurrentAnimatorClipInfo(0);
            string animationName = "";
            if (animatorInfo.Length > 0)
            {
                animationName = animatorInfo[0].clip.name;
            }

            switch (animationName)
            {
                case "Lifting":
                    InAnimation = true;
                    break;
                case "Waving":
                    InAnimation = true;
                    break;
                case "Tut Hip Hop Dance":
                    InAnimation = true;
                    break;
                case "":
                    InAnimation = true;
                    break;
                default:
                    InAnimation = false;
                    break;
            }
        }
    }

    public void AnimateContinuousAnimation(KeyCode key, bool pressed, bool run)
    {
        //klettern
        if (key == KeyCode.C && pressed && !InAnimation)
        {
            Animator.SetBool("Tedclimb", true);
        }
        if (key == KeyCode.C && !pressed && !InAnimation) 
        {
            Animator.SetBool("Tedclimb", false);
        }
        //laufen und rennen
        if ((key == KeyCode.W || key == KeyCode.A || key == KeyCode.S || key == KeyCode.D) && pressed && !InAnimation)
        {
            if (run)
            {
                Animator.SetBool("Tedrun", true);
                Animator.SetBool("Tedwalk", false);
            }
            else
            {
                Animator.SetBool("Tedwalk", true);
                Animator.SetBool("Tedrun", false);
            }
        }
        else
        {
            Animator.SetBool("Tedwalk", false);
            Animator.SetBool("Tedrun", false);
        }
    }

    public void AnimateSingleAnimation(KeyCode key) 
    {
        //springen
        if (key == KeyCode.Space && !InAnimation)
        {
            Animator.SetTrigger("TedJump");
            InAnimation = true;
        }
        //interaktion/aufheben
        else if (key == KeyCode.F && !InAnimation)
        {
            Animator.SetTrigger("TedPickup");
            InAnimation = true;
        }
        //winken
        else if (key == KeyCode.G && !InAnimation)
        {
            Animator.SetTrigger("TedWave");
            InAnimation = true;
        }
        //tanzen
        else if (key == KeyCode.J && !InAnimation)
        {
            Animator.SetTrigger("TedDance");
            InAnimation = true;
        }
    }
}
