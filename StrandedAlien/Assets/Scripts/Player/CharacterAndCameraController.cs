﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterAndCameraController : MonoBehaviour
{
    [Header("")]
    [SerializeField]
    [Tooltip("")]
    private float MovementSpeed = 10.0f;
    [SerializeField]
    [Tooltip("")]
    private float RotationSpeed = 10.0f;
    [SerializeField]
    [Tooltip("")]
    private float JumpHeight = 5.0f;

    [SerializeField]
    [Tooltip("")]
    private GameObject CameraTargert;
    [SerializeField]
    [Tooltip("")]
    private GameObject GameFlow;

    [SerializeField]
    [Tooltip("")]
    private LayerMask GroundLayer;
    [Tooltip("")]
    [SerializeField]
    private float GroundedOffset = 0.25f;
    [SerializeField]
    [Tooltip("")]
    private float Gravity = Physics.gravity.y;
    [SerializeField]
    [Tooltip("")]
    private bool Grounded = true;

    [SerializeField]
    [Tooltip("")]
    private Transform CrosshairImage;

    private CharacterController _CharacterController;
    private float verticalVelocity;
    private bool jump;

    private GameObject _MainCamera;
    private bool CameraFixed;
    private float cameraYMovement, cameraXMovement;

    private List<IInteractable> InteractableItems = new List<IInteractable>();
    private List<ITargetable> TargetedItems = new List<ITargetable>();

    private InventoryController _InventoryController;
    private GameFlowController _GameFlowController;
    private QuestControllerV2 _QuestController;


    private void Awake()
    {
        if (_MainCamera == null)
        {
            _MainCamera = Camera.main.gameObject;
        }
    }

    private void Start()
    {
        _CharacterController = GetComponent<CharacterController>();
        _InventoryController = GetComponent<InventoryController>();
        _GameFlowController = GameFlow.GetComponent<GameFlowController>();
        _QuestController = GameFlow.GetComponent<QuestControllerV2>();
    }

    private void Update()
    {
        if (_GameFlowController.inputEnabled) 
        {
            //check if jump button has been pressed in this frame
            jump = Input.GetButtonDown("Jump");
        }
        //performe character interaction
        Interaction();
        //performe ground check
        GroundedCheck();
        //calculate jump and gravity
        JumAndGravity();
        //performe character movement
        CharacterMovement();
    }

    private void LateUpdate()
    {
        //performe camera movement
        if (!CameraFixed) 
        {
            CameraMovement();
            TriggerfacingObject();
        }
    }

    public void AddInteractableItem(IInteractable interactable)
    {
        if (!InteractableItems.Contains(interactable))
        {
            InteractableItems.Add(interactable);
        }
    }

    public Vector3 GetCrosshairPosition()
    {
        return CrosshairImage.position;
    }

    public void SetCameraFixed(bool cameraFixed)
    {
        this.CameraFixed = cameraFixed;
    }



    private void CharacterMovement() 
    {
        Vector3 characterMovement = Vector3.zero;

        if (_GameFlowController.inputEnabled)
        {
            characterMovement = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            characterMovement *= MovementSpeed;

            characterMovement.y = verticalVelocity;

        }

        //transform.rotation -> dreht den charkter so dass W immer nach vorne ist 
        _CharacterController.Move(transform.rotation * characterMovement * Time.deltaTime);
    }

    private void CameraMovement() 
    {
        if (_GameFlowController.inputEnabled)
        {
            cameraXMovement += Input.GetAxis("Mouse X") * RotationSpeed;
            cameraYMovement -= Input.GetAxis("Mouse Y") * RotationSpeed;

            cameraYMovement = Mathf.Clamp(cameraYMovement, -35, 60);


            CameraTargert.transform.rotation = Quaternion.Euler(cameraYMovement, cameraXMovement, 0.0f);
            transform.rotation = Quaternion.Euler(0.0f, cameraXMovement, 0.0f);

            _MainCamera.transform.LookAt(CameraTargert.transform);
        }
    }

    private void GroundedCheck() 
    {
        Vector3 characterPositionWithOffset = new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z);
        Grounded = Physics.CheckSphere(characterPositionWithOffset, (_CharacterController.height / 2), GroundLayer, QueryTriggerInteraction.Ignore);
    }

    private void Interaction() 
    {
        if (InteractableItems.Count > 0 && _GameFlowController.inputEnabled && Input.GetMouseButtonDown(0))
        {
            IInteractable interactable = InteractableItems[0];

            if (interactable is IItem item)
            {
                if (item is Spare && !((Spare)item).RequiredEquipment.Equals(EnumEquipment.Default) && !_InventoryController.EquipmentAvailable(((Spare)item).RequiredEquipment))
                {
                    return;
                }
                else
                {
                    _InventoryController.AddItemToInventory(item);
                    _QuestController.UpdateQuestState();
                    ((Item)item).gameObject.SetActive(false);
                }
            }
            else
            {
                interactable.Interaction();
            }
        }
        InteractableItems.Clear();
    }

    private void JumAndGravity() 
    {
        if (Grounded)
        {
            if (verticalVelocity < 0.0f)
            {
                verticalVelocity = -3;
            }

            if (jump )
            {
                verticalVelocity = Mathf.Sqrt(JumpHeight * -3.0f * Gravity);
            }
        }
        else
        {
            verticalVelocity += Gravity * 2.5f * Time.deltaTime;
            jump = false;
        }
    }

    private void TriggerfacingObject()
    {
        bool hitsTargetable = false;

        Ray ray = _MainCamera.GetComponent<Camera>().ScreenPointToRay(CrosshairImage.position);
        RaycastHit[] hitsArray = Physics.RaycastAll(ray.origin, ray.direction, 8f);
        List<RaycastHit> hits = hitsArray.ToList();

        if (hits.Any())
        {
            foreach (RaycastHit hit in hits)
            {
                if (hit.transform.TryGetComponent(out IInteractable interactable))
                {
                    AddInteractableItem(interactable);
                }
                if (hit.transform.TryGetComponent(out ITargetable targetable))
                {
                    targetable.Targeted(true);
                    TargetedItems.Add(targetable);
                    hitsTargetable = true;
                }
            }
        }

        if (!hits.Any() || !hitsTargetable)
        {
            if (TargetedItems.Any())
            {
                ClearTargetedItems();
            }
        }
    }

    private void ClearTargetedItems()
    {
        foreach (ITargetable item in TargetedItems)
        {
            item.Targeted(false);
        }
        TargetedItems.Clear();
    }
}
