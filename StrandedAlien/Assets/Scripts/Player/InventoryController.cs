﻿using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    private readonly List<EnumEquipment> AvailableEquipment = new List<EnumEquipment>();
    private readonly SortedDictionary<EnumSpare, int> AvailableSpare = new SortedDictionary<EnumSpare, int>();

    public void AddItemToInventory(IItem item) 
    {
        if (item is Spare)
        {
            AddSpare(((Spare)item).SpareType, 1);
        }
        else if (item is Equipment)
        {
            AddEquipment(((Equipment)item).EquipmentType);
        }
    }

    public bool EquipmentAvailable(EnumEquipment equipment) 
    {
        if (equipment != EnumEquipment.Default) 
        {
            return AvailableEquipment.Contains(equipment);
        }
        return false;
    }

    public int GetSpareAmount(EnumSpare spare) 
    {
        if (AvailableSpare.ContainsKey(spare) && spare != EnumSpare.Default) 
        {
            return AvailableSpare[spare];
        }
        return 0;
    }

    public void RemoveSpare(EnumSpare spare, int amount) 
    {
        if (spare != EnumSpare.Default && AvailableSpare.ContainsKey(spare))
        {
            int inventorySpareAmount = AvailableSpare[spare];
            if (inventorySpareAmount > amount)
            {
                AvailableSpare[spare] -= amount;
            }
            else if (inventorySpareAmount == amount) 
            {
                AvailableSpare.Remove(spare);
            }
            else if (inventorySpareAmount < amount) 
            {
                Debug.LogError("Error in " + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name
                        + " in method " + System.Reflection.MethodBase.GetCurrentMethod().Name);
                Debug.LogError("Trying to use more equipment then available");
            }
            
        }
    }

    private void AddEquipment(EnumEquipment equipment)
    {
        if (!AvailableEquipment.Contains(equipment) && equipment != EnumEquipment.Default)
        {
            AvailableEquipment.Add(equipment);
        }
    }

    private void AddSpare(EnumSpare spare, int amount)
    {
        if (spare.Equals(EnumSpare.PowerCellCharge))
        {
            amount *= 20;
        }
        if (spare != EnumSpare.Default)
        {
            if (AvailableSpare.ContainsKey(spare))
            {
                AvailableSpare[spare] += amount;
            }
            else
            {
                AvailableSpare.Add(spare, amount);
            }
        }
    }
}
