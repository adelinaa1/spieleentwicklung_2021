﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceshipController : MonoBehaviour, IInteractable, ITargetable
{
    public GameObject GameFlow;
    public GameObject Player;
    public GameObject UICanvas;

    private QuestControllerV2 QuestController;
    private UIController UIController;
    private Dictionary<EnumQuest, bool> SpaceshipPartRepaireState;
    private GameObject DomeDamaged;
    private GameObject DomeRepaired;
    private GameObject UpperPartDamaged;
    private GameObject UpperPartRepaired;
    private GameObject LegDamaged;
    private GameObject LegRepaired;
    private GameObject AntennaDamaged;
    private GameObject AntennaRepaired;
    private GameObject PowerCellDamaged;
    private GameObject PowerCellRepaired;
    private Text Text;
    private Image Background;

    private readonly int INFO_TEXT_TIME = 3;
    private readonly int REPAIR_BLACK_SCREEN_TIME = 4;
    void Start()
    {
        QuestController = GameFlow.GetComponent<QuestControllerV2>();
        UIController = UICanvas.GetComponent<UIController>();
        Player.GetComponent<InventoryController>();

        DomeDamaged = transform.Find("Dome/DomeDamaged").gameObject;
        DomeRepaired = transform.Find("Dome/DomeRepaired").gameObject;
        UpperPartDamaged = transform.Find("UpperPart/UpperPartDamaged").gameObject;
        UpperPartRepaired = transform.Find("UpperPart/UpperPartRepaired").gameObject;
        LegDamaged = transform.Find("Leg/LegDamaged").gameObject;
        LegRepaired = transform.Find("Leg/LegRepaired").gameObject;
        AntennaDamaged = transform.Find("Antenna/AntennaDamaged").gameObject;
        AntennaRepaired = transform.Find("Antenna/AntennaRepaired").gameObject;
        PowerCellDamaged = transform.Find("PowerCell/PowerCellDamaged").gameObject;
        PowerCellRepaired = transform.Find("PowerCell/PowerCellRepaired").gameObject;

        Text = UICanvas.transform.Find("ItemText/Panel/Text").GetComponent<Text>();
        Background = UICanvas.transform.Find("ItemText/Panel").GetComponent<Image>();

        SpaceshipPartRepaireState = new Dictionary<EnumQuest, bool>();
        SpaceshipPartRepaireState.Add(EnumQuest.Holes, false);
        SpaceshipPartRepaireState.Add(EnumQuest.Leg, false);
        SpaceshipPartRepaireState.Add(EnumQuest.Antenna, false);
        SpaceshipPartRepaireState.Add(EnumQuest.PowerCell, false);

        //kaputte teile aktivieren
        DomeDamaged.SetActive(true);
        UpperPartDamaged.SetActive(true);
        LegDamaged.SetActive(true);
        AntennaDamaged.SetActive(true);
        PowerCellDamaged.SetActive(true);
        //ganze teile deaktivieren
        DomeRepaired.SetActive(false);
        UpperPartRepaired.SetActive(false);
        LegRepaired.SetActive(false);
        AntennaRepaired.SetActive(false);
        PowerCellRepaired.SetActive(false);
    }

    public bool SpaceshipPartRepaired(EnumQuest part)
    {
        if (SpaceshipPartRepaireState.ContainsKey(part))
        {
            return SpaceshipPartRepaireState[part];
        }
        return false;
    }

    public int RepairSpaceship() 
    {
        
        EnumQuest state = QuestController.GetState();
        switch (state)
        {
            case EnumQuest.Holes:
                //die kuppel und der obere teil des rauschiffs sind repariert wenn sowohl der kaputte wie auch der raparierte Teil aktiv sind
                DomeDamaged.SetActive(true);
                DomeRepaired.SetActive(true);
                UpperPartDamaged.SetActive(true);
                UpperPartRepaired.SetActive(true);
                QuestController.RemoveQuestItemsFromInventory();
                SpaceshipPartRepaireState[EnumQuest.Holes] = true;
                QuestController.SetInfoText(false);
                break;
            case EnumQuest.Leg:
                LegDamaged.SetActive(false);
                LegRepaired.SetActive(true);
                QuestController.RemoveQuestItemsFromInventory();
                SpaceshipPartRepaireState[EnumQuest.Leg] = true;
                QuestController.SetInfoText(false);
                break;
            case EnumQuest.Antenna:
                //die antenne ist repariert wenn sowohl der kaputte als auch der reparierte Teil aktiv ist
                AntennaDamaged.SetActive(true);
                AntennaRepaired.SetActive(true);
                QuestController.RemoveQuestItemsFromInventory();
                SpaceshipPartRepaireState[EnumQuest.Antenna] = true;
                QuestController.SetInfoText(false);
                break;
            case EnumQuest.PowerCell:
                PowerCellDamaged.SetActive(false);
                PowerCellRepaired.SetActive(true);
                QuestController.RemoveQuestItemsFromInventory();
                SpaceshipPartRepaireState[EnumQuest.PowerCell] = true;
                QuestController.SetInfoText(false);
                break;
            //dieser case sollte folgende stati abdecken:
            //EnumQuest.Default
            //EnumQuest.Start
            //EnumQuest.Ende
            //EnumQuest.Flashlight
            //EnumQuest.Wrench
            //EnumQuest.Knife
            //in diesen fällen muss nichts gemacht werden.
            default:
                //der fall sollte nie erreicht werden
                return 1;
        }
        return 0;
    }

    public void Interaction()
    {
        EnumQuest state = QuestController.GetState();
        if (state.Equals(EnumQuest.End)) 
        {
            UIController.GameEnd();
        }
        else if (state.Equals(EnumQuest.Start) || state.Equals(EnumQuest.Default) || state.Equals(EnumQuest.Flashlight) || state.Equals(EnumQuest.Knife) || state.Equals(EnumQuest.Wrench))// || state.Equals(EnumQuest.Ende)) 
        {
            UIController.ShowUIElement(EnumUI.InfoTextNoRepair, INFO_TEXT_TIME, TextsV2.NoRapair);
            return;
        }
        else if (!QuestController.QuestItemsFound())
        {
            UIController.ShowUIElement(EnumUI.InfoTextRepairNotPossible, INFO_TEXT_TIME, TextsV2.RepairNotPossibleText);
            return;
        }
        else if (state.Equals(EnumQuest.Holes) || state.Equals(EnumQuest.Leg) || state.Equals(EnumQuest.Antenna) || state.Equals(EnumQuest.PowerCell)) 
        {
            //UIController.ShowUIElement(EnumUI.Overlay, REPAIR_BLACK_SCREEN_TIME);
            UIController.ShowOverlay();
        }        
    }

    public void Targeted(bool isTargeted)
    {
        if (isTargeted)
        {
            Text.text = TextsV2.SpaceshipText;
            //set position
            //Transform crosshair = Player.transform.Find("Crosshair");
            Vector3 pos = Player.GetComponent<CharacterAndCameraController>().GetCrosshairPosition();
            float scale = UICanvas.GetComponent<Canvas>().scaleFactor;
            pos.x -= Text.preferredWidth * scale * 0.75f;
            Background.transform.position = pos;
        }
        Background.enabled = isTargeted;
        Text.enabled = isTargeted;
    }
}
