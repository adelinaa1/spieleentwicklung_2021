﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameFlowController : MonoBehaviour
{
    [SerializeField]
    private GameObject Player;
    [SerializeField]
    private GameObject UI;
    private UIController UIController;
    [SerializeField]
    private float FadeInOutDuration;

    public bool inputEnabled { private set; get; }
    private int TimerID;

    void Start()
    {
        UIController = UI.GetComponent<UIController>();
        inputEnabled = true;
        TimerID = 0;
        if (FadeInOutDuration.Equals(0)) 
        {
            FadeInOutDuration = 1.2f;
        }
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void SetInputEnabled(bool value)
    {
        inputEnabled = value;
    }

    public void PauseGame()
    {
        AudioManager.instance.PauseCurrentAudio();
        Time.timeScale = 0;
        UIController.DeactivateUIController();
        Player.GetComponent<CharacterAndCameraController>().SetCameraFixed(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void ResumeGame()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Player.GetComponent<CharacterAndCameraController>().SetCameraFixed(false);
        UIController.ActivateUIController();
        Time.timeScale = 1;
        AudioManager.instance.ResumeCurrentAudio();
    }

    public Timer GenerateTimer() 
    {
        Timer tmpTimer = gameObject.AddComponent<Timer>();
        tmpTimer.ID = TimerID;
        TimerID++;
        return tmpTimer;
    }

    public void DeleteTimer(Timer timer)
    {
        Timer[] timers = gameObject.GetComponents<Timer>();
        foreach (Timer tmpTimer in timers) 
        {
            if (timer.ID.Equals(tmpTimer.ID)) 
            {
                Destroy(timer);
                break;
            }
        }
    }

    public IEnumerator FadeInTextAndImageOfGameObject(GameObject gameObject, Func<int> callback = null)
    {
        bool conditionMet = false;
        Text gameObjectText;
        Image gameObjectImage;
        Color tmpColor;
        if (!gameObject.TryGetComponent(out gameObjectImage))
        {
            gameObjectImage = gameObject.GetComponentInChildren<Image>();
        }
        if (!gameObject.TryGetComponent(out gameObjectText))
        {
            gameObjectText = gameObject.GetComponentInChildren<Text>();
        }
        if (gameObjectImage != null) 
        {
            tmpColor = gameObjectImage.color;
            tmpColor.a = 0;
            gameObjectImage.color = tmpColor;
        }
        if (gameObjectText != null) 
        {
            tmpColor = gameObjectText.color;
            tmpColor.a = 0;
            gameObjectText.color = tmpColor;
        }
        if (gameObject.activeSelf == false || gameObject.activeInHierarchy == false)
        {
            gameObject.SetActive(true);
        }

        for (float i = 0; i < 1; i += Time.deltaTime * FadeInOutDuration)
        {
            if (!conditionMet)
            {
                if (gameObjectText != null)
                {
                    tmpColor = gameObjectText.color;
                    tmpColor.a = i;
                    gameObjectText.color = tmpColor;
                }
                if (gameObjectImage != null)
                {
                    tmpColor = gameObjectImage.color;
                    tmpColor.a = i;
                    gameObjectImage.color = tmpColor;
                }
                if (i >= 0.9)
                {
                    conditionMet = true;
                    if (gameObjectText != null)
                    {
                        tmpColor = gameObjectText.color;
                        tmpColor.a = 1;
                        gameObjectText.color = tmpColor;
                    }
                    if (gameObjectImage != null)
                    {
                        tmpColor = gameObjectImage.color;
                        tmpColor.a = 1;
                        gameObjectImage.color = tmpColor;
                    }
                }
            }
            yield return null;
        }
        callback?.Invoke();
    }

    public IEnumerator FadeOutTextAndImageOfGameObject(GameObject gameObject, bool deactivate, Func<int> callback = null)
    {
        bool conditionMet = false;
        Text gameObjectText;
        Image gameObjectImage;
        Color tmpColor;
        if (!gameObject.TryGetComponent(out gameObjectImage))
        {
            gameObjectImage = gameObject.GetComponentInChildren<Image>();
        }
        if (!gameObject.TryGetComponent(out gameObjectText)) 
        {
            gameObjectText = gameObject.GetComponentInChildren<Text>();
        }
        if (gameObjectImage != null)
        {
            tmpColor = gameObjectImage.color;
            tmpColor.a = 1;
            gameObjectImage.color = tmpColor;
        }
        if (gameObjectText != null)
        {
            tmpColor = gameObjectText.color;
            tmpColor.a = 1;
            gameObjectText.color = tmpColor;
        }
        for (float i = 1; i > 0; i -= Time.deltaTime * FadeInOutDuration)
        {
            if (!conditionMet)
            {
                if (gameObjectText != null)
                {
                    tmpColor = gameObjectText.color;
                    tmpColor.a = i;
                    gameObjectText.color = tmpColor;
                }
                if (gameObjectImage != null) 
                {
                    tmpColor = gameObjectImage.color;
                    tmpColor.a = i;
                    gameObjectImage.color = tmpColor;
                }
                if (i <= 0.1)
                {
                    conditionMet = true;
                    if (gameObjectText != null)
                    {
                        tmpColor = gameObjectText.color;
                        tmpColor.a = 0;
                        gameObjectText.color = tmpColor;
                    }
                    if (gameObjectImage != null)
                    {
                        tmpColor = gameObjectImage.color;
                        tmpColor.a = 0;
                        gameObjectImage.color = tmpColor;
                    }
                    if (deactivate)
                    {
                        gameObject.SetActive(false);
                    }
                }
            }
            yield return null;
        }
        callback?.Invoke();
    }
}
