﻿public static class TextsV2
{
    public static readonly string REPAIR_TEXT = "repairText";
    //info texte
    /*public static string QuestCompleteText { private set; get; } =
        "Quest abgeschlossen.";*/
    public static string QuestItemsFoundText { private set; get; } =
        "Alle Gegenstände gefunden";
    public static string RepairNotPossibleText { private set; get; } =
        "Zur Reparatur werden\n" +
        "weitere Teile benötigt";
    public static string NoRapair { private set; get; } =
        "Es kann nichts repariert werden";
    //steuerung
    public static string Controls { private set; get; } =
        "Laufen: W,A,S,D\n" +
        "Springen: Leertaste\n" +
        "Kamera: Maus\n" +
        "Interaktion/Aufheben: Linke Maustaste\n" +
        "Menü: Escape\n";

    //tablet texte
    /*public static string StartText { private set; get; } =
        "Hallo Ted,\nich bin XXX deine persönliche Assistentin.\n" +
        "Mit Sicherheit ist dir aufgefallen, dass das Raumschiff abgestürzt ist.\n" +
        "Um deine Reise fortsetzen zu können musst du das Raumschiff reparieren.\n" +
        "Meine Scans haben ergeben, dass das nötige Material für die Reparatur in der Nähe zu finden ist.\n\n" +
        "Der Status der aktuellen Quest und zusätzliche Informationen werden im Tablet angezeigt.";*/
    /*public static string EndeText { private set; get; } =
        "Das Raumschiff ist repariert und wir können den Planeten verlassen.\n\n" +
        "Gehe zum Raumschiff und steige ein (Interaktion) wenn du bereit bist den Planeten zu verlassen.";*/
    public static string RepairText { private set; get; } =
        "Kehre zum Raumschiff zurück und führe die Reparatur durch.";
    /*public static string RepairDoneText { private set; get; } =
        "Reperatur abgeschlossen.";*/
    //quest texte
    /*public static string QuestFlashlightText { private set; get; } =
        "Da es bald dunkel wird, solltest du zuerst eine Taschenlampe finden um dich in der Dunkelheit zurecht zu finden.\n";
    public static string QuestKnifeText { private set; get; } =
        "Um Kabel und Folie, die zur Reparatur benötigt werden, bearbeiten zu können, benötigst du ein Messer.\n";
    public static string QuestWrenchText { private set; get; } =
        "Um die beschädigte Antenne Reparieren zu können benötigst du einen Schraubenschlüssel.\n";
    public static string QuestHolesText { private set; get; } =
        "Beim Aufprall sind mehrere Löcher in der Hülle entstanden. Diese können mit Klebeband abgedichtet werden.\n";
    public static string QuestLegText { private set; get; } =
        "Eines der Beine ist abgebrochen. Zur Reparatur benötigst du ein Rohr, eine Venylplatte und Klebeband.";
    public static string QuestAntennaText { private set; get; } =
        "Die Antenne des Raumschiffs ist beschädigt.\n" +
        "An einem alten Radio solltest du eine neue finden können.\n" +
        "Außerdem benötigst du Kabel um sie anzuschließen und Folie zur Isolation.";
    public static string QuestPowerCellText { private set; get; } =
        "Die Energiezelle wurde zerstört.\n" +
        "Du solltest eine neue finden und die Energie der anderen Raumschiffe nutzen um die Energiezelle aufzuladen.";*/
    //finde ausrüstung quest texte
    public static string FindFlashlight { private set; get; } =
        "Finde Taschenlampe: ";
    public static string FindKnife { private set; get; } =
        "Finde Messer: ";
    public static string FindWrench { private set; get; } =
        "Finde Schraubenschlüssel: ";
    //finde ersatzteile quest texte
    public static string FindCabel { private set; get; } =
        "Finde Kabel: ";
    public static string FindTape { private set; get; } =
        "Finde Klebeband: ";
    public static string FindFoil { private set; get; } =
        "Finde Folie: ";
    public static string FindPipe { private set; get; } =
        "Finde Rohr: ";
    public static string FindPowerCell { private set; get; } =
        "Finde Energiezelle: ";
    public static string FindPowerCellCharge { private set; get; } =
        "Ladung der Energiezelle: ";
    public static string FindAntenna { private set; get; } =
        "Finde Antenne: ";
    public static string FindVinyl { private set; get; } =
        "Finde Venylplatte: ";
    
    //raumschiff text
    public static string SpaceshipText { private set; get; } =
        "Raumschiff";

    //item texte
    //equipment
    public static string FlashlightInfoText { private set; get; } =
        "Taschenlampe\n" +
        "Bringt Licht ins Dunkel";
    public static string FlashlightErrorText { private set; get; } =
        "<color=red>Taschenlampe-ERROR</color>";
    public static string KnifeInfoText { private set; get; } =
        "Messer\n" +
        "Benötigt zum Schneiden";
    public static string KnifeErrorText { private set; get; } =
        "<color=red>Messer-ERROR</color>";
    public static string WrenchInfoText { private set; get; } =
        "Schraubenschlüssel\n" +
        "Benötigt für Reparaturen";
    public static string WrenchErrorText { private set; get; } =
        "<color=red>Schraubenschlüssel-ERROR</color>";
    //spare
    public static string AntennaInfoText { private set; get; } =
        "Antenne\n" +
        "Ersatzteil zur Reparatur";
    public static string AntennaErrorText { private set; get; } =
        "<color=red>Schraubenschlüssel benötigt</color>";
    public static string CableInfoText { private set; get; } =
        "Kabel\n" +
        "Ersatzteil zur Reparatur";
    public static string CableErrorText { private set; get; } =
        "<color=red>Messer benötigt</color>";
    public static string FoilInfoText { private set; get; } =
        "Folie\n" +
        "Ersatzteil zur Reparatur";
    public static string FoilErrorText { private set; get; } =
        "<color=red>Messer benötigt</color>";
    public static string PipeInfoText { private set; get; } =
        "Rohr\n" +
        "Ersatzteil zur Reparatur";
    public static string PipeErrorText { private set; get; } =
        "<color=red>Rohr-ERROR</color>";
    public static string PowerCellInfoText { private set; get; } =
        "Energiezelle\n" +
        "Ersatzteil zur Reparatur";
    public static string PowerCellErrorText { private set; get; } =
        "<color=red>Energiezelle-ERROR</color>";
    public static string TapeInfoText { private set; get; } =
        "Klebeband\n" +
        "Ersatzteil zur Reparatur";
    public static string TapeErrorText { private set; get; } =
        "<color=red>Messer benötigt</color>";
    public static string VinylInfoText { private set; get; } =
        "Vinyl\n" +
        "Ersatzteil zur Reparatur";
    public static string VinylErrorText { private set; get; } =
        "<color=red>Vinyl-ERROR</color>";
    public static string PowerCellChargeText { private set; get; } =
        "Batterie\n" +
        "Ladung für Energiezelle";
    public static string PowerCellChargeErrorText { private set; get; } =
        "<color=red>Batterie-ERROR</color>";


    public static string GetStringNames(EnumSpare spare = EnumSpare.Default, EnumEquipment equipment = EnumEquipment.Default)
    {
        if (!equipment.Equals(EnumEquipment.Default) && spare.Equals(EnumSpare.Default))
        {
            switch (equipment)
            {
                case EnumEquipment.Flashlight:
                    return nameof(FindFlashlight);
                case EnumEquipment.Wrench:
                    return nameof(FindWrench);
                case EnumEquipment.Knife:
                    return nameof(FindKnife);
                case EnumEquipment.Default:
                    return "";
                default:
                    return "";
            }
        }
        else if (!spare.Equals(EnumSpare.Default) && equipment.Equals(EnumEquipment.Default))
        {
            switch (spare)
            {
                case EnumSpare.Tape:
                    return nameof(FindTape);
                case EnumSpare.Cable:
                    return nameof(FindCabel);
                case EnumSpare.Foil:
                    return nameof(FindFoil);
                case EnumSpare.Pipe:
                    return nameof(FindPipe);
                case EnumSpare.Antenna:
                    return nameof(FindAntenna);
                case EnumSpare.Vinyl:
                    return nameof(FindVinyl);
                case EnumSpare.PowerCell:
                    return nameof(FindPowerCell);
                case EnumSpare.PowerCellCharge:
                    return nameof(FindPowerCellCharge);
                case EnumSpare.Default:
                    return "";
                default:
                    return "";
            }
        }
        else
        {
            return "";
        }
    }
}
