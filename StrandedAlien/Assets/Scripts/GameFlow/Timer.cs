﻿using System;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public int ID { set; get; }
    private bool TimerActive;
    private float TimerTime;

    void Start()
    {
        TimerActive = false;
        TimerTime = 0;
    }

    void Update()
    {
        if (TimerActive) 
        {
            TimerTime += Time.deltaTime;
        }
        
    }

    public void StartTimer() 
    {
        TimerActive = true;
    }

    public void StopTimer(bool reset) 
    {
        TimerActive = false;
        if (reset) 
        {
            TimerTime = 0;
        }
    }

    public void ResetTimer() 
    {
        TimerTime = 0;
    }

    public float GetTimerTime() 
    {
        return TimerTime;
    }

    public int GetTimerTimeAsInt()
    {
        return Mathf.FloorToInt(TimerTime);
    }
}
