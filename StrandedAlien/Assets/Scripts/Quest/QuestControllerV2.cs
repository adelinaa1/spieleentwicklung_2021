﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestControllerV2 : MonoBehaviour
{
    [SerializeField]
    private GameObject Player;
    private InventoryController InventoryController;

    [SerializeField]
    private GameObject Spaceship;
    private SpaceshipController SpaceshipController;

    [SerializeField]
    private GameObject UI;
    private UIController UIController;
    private UIQuestController UIQuestController;

    private EnumQuest State;
    private EnumQuest ItemsFoundState;
    private List<QuestStruct> Quests;
    private Dictionary<EnumEquipment, bool> Equipment;
    private List<EnumQuest> QuestOrder;
    private bool InfoText;

    private readonly int INFO_TEXT_TIME = 3;

    private void Start()
    {
        InventoryController = Player.GetComponent<InventoryController>();
        SpaceshipController = Spaceship.GetComponent<SpaceshipController>();
        UIController = UI.GetComponent<UIController>();
        UIQuestController = UI.GetComponentInChildren<UIQuestController>();

        Quests = new List<QuestStruct>();
        Equipment = new Dictionary<EnumEquipment, bool>();
        QuestOrder = new List<EnumQuest>();
        InitQuestController();
    }

    private void Update()
    {
        if(State.Equals(EnumQuest.Start) && !AudioManager.instance.IsAudioInQueueOrPlaying()) 
        {
            UpdateQuestState();
        }
        if ((State.Equals(EnumQuest.Knife) || State.Equals(EnumQuest.Flashlight) || State.Equals(EnumQuest.Wrench)) && QuestComplete())
        {
            UpdateQuestState();
        }
        //notification that all items are found
        else if (QuestItemsFound() && InfoText.Equals(false) && !ItemsFoundState.Equals(State))
        {
            UIController.ShowUIElement(EnumUI.InfoTextQuestItemsFound, INFO_TEXT_TIME, TextsV2.QuestItemsFoundText);
            InfoText = true;
            ItemsFoundState = State;
        }
    }

    public int UpdateQuestState()
    {
        if (State.Equals(EnumQuest.Default)) 
        {
            Debug.LogError("Error in " + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name
                       + " in method " + System.Reflection.MethodBase.GetCurrentMethod().Name);
            Debug.LogError("Quest state can not be changed because it equals default.");
            return 1;
        }
        if (State.Equals(EnumQuest.End))
        {
            Debug.Log("ENDE");
            return 1;
        }

        if (!QuestComplete())
        {
            UIQuestController.UIQuestText();
            return 1;
        }
        
        int index = QuestOrder.IndexOf(State);
        if (index < QuestOrder.Count - 1)
        {
            index++;
            State = QuestOrder[index];
            if (!QuestItemsFound()) 
            {
                PlayQuestAudio(State);
            }
            SetInfoText(false);
            UIQuestController.UIQuestText();
            return 0;
        }
        return 1;
    }

    public bool QuestItemsFound()
    {
        if (State.Equals(EnumQuest.Flashlight) || State.Equals(EnumQuest.Knife) || State.Equals(EnumQuest.Wrench))
        {
            switch (State)
            {
                case EnumQuest.Flashlight:
                    return InventoryController.EquipmentAvailable(EnumEquipment.Flashlight);
                case EnumQuest.Knife:
                    return InventoryController.EquipmentAvailable(EnumEquipment.Knife);
                case EnumQuest.Wrench:
                    return InventoryController.EquipmentAvailable(EnumEquipment.Wrench);
                default:
                    return false;
            }
        }
        else
        {
            bool allItems = true;
            QuestStruct currentQuest = GetCurrentQuest();
            if (currentQuest.ID.Equals(EnumQuest.Default))
            {
                return false;
            }
            IEnumerator enumerator = currentQuest.GetQuestItems();

            while (enumerator.MoveNext())
            {
                EnumSpare spare = (EnumSpare)enumerator.Current;
                int questSpareAmount = currentQuest.GetQuestItemAmount(spare);
                int inventorySpareAmount = InventoryController.GetSpareAmount(spare);
                if (inventorySpareAmount >= questSpareAmount)
                {
                    allItems &= true;
                }
                else
                {
                    allItems &= false;
                }
            }

            return allItems;
        }
    }

    public EnumQuest GetState()
    {
        return State;
    }

    public void RemoveQuestItemsFromInventory()
    {
        QuestStruct currentQuest = GetCurrentQuest();
        if (currentQuest.ID.Equals(EnumQuest.Default))
        {
            return;
        }

        IEnumerator enumerator = currentQuest.GetQuestItems();
        while (enumerator.MoveNext())
        {
            EnumSpare spare = (EnumSpare)enumerator.Current;
            int questAmount = currentQuest.GetQuestItemAmount(spare);
            InventoryController.RemoveSpare(spare, questAmount);
        }
    }

    public void SetInfoText(bool infoText) 
    {
        InfoText = infoText;
    }

    public Dictionary<string, string> GetQuestText()
    {
        Dictionary<string, string> textDictionary = new Dictionary<string, string>();
        switch (State)
        {
            case EnumQuest.Flashlight:
                textDictionary.Add(TextsV2.GetStringNames(equipment: EnumEquipment.Flashlight), GetFindText(TextsV2.FindFlashlight, equipment: EnumEquipment.Flashlight));
                return textDictionary;
            case EnumQuest.Knife:
                textDictionary.Add(TextsV2.GetStringNames(equipment: EnumEquipment.Knife), GetFindText(TextsV2.FindKnife, equipment: EnumEquipment.Knife));
                return textDictionary;
            case EnumQuest.Wrench:
                textDictionary.Add(TextsV2.GetStringNames(equipment: EnumEquipment.Wrench), GetFindText(TextsV2.FindWrench, equipment: EnumEquipment.Wrench));
                return textDictionary;
            case EnumQuest.Holes:
                if (QuestItemsFound() && !SpaceshipController.SpaceshipPartRepaired(State))
                {
                    textDictionary.Add(TextsV2.REPAIR_TEXT, TextsV2.RepairText);
                }
                else
                {
                    textDictionary.Add(TextsV2.GetStringNames(spare: EnumSpare.Tape), GetFindText(TextsV2.FindTape, spare: EnumSpare.Tape));
                }
                return textDictionary;
            case EnumQuest.Leg:
                if (QuestItemsFound() && !SpaceshipController.SpaceshipPartRepaired(State))
                {
                    textDictionary.Add(TextsV2.REPAIR_TEXT, TextsV2.RepairText);
                }
                else
                {
                    textDictionary.Add(TextsV2.GetStringNames(spare: EnumSpare.Pipe), GetFindText(TextsV2.FindPipe, spare: EnumSpare.Pipe));
                    textDictionary.Add(TextsV2.GetStringNames(spare: EnumSpare.Vinyl), GetFindText(TextsV2.FindVinyl, spare: EnumSpare.Vinyl));
                    textDictionary.Add(TextsV2.GetStringNames(spare: EnumSpare.Tape), GetFindText(TextsV2.FindTape, spare: EnumSpare.Tape));
                }
                return textDictionary;
            case EnumQuest.Antenna:
                if (QuestItemsFound() && !SpaceshipController.SpaceshipPartRepaired(State))
                {
                    textDictionary.Add(TextsV2.REPAIR_TEXT, TextsV2.RepairText);
                }
                else 
                {
                    textDictionary.Add(TextsV2.GetStringNames(spare: EnumSpare.Cable), GetFindText(TextsV2.FindCabel, spare: EnumSpare.Cable));
                    textDictionary.Add(TextsV2.GetStringNames(spare: EnumSpare.Foil), GetFindText(TextsV2.FindFoil, spare: EnumSpare.Foil));
                    textDictionary.Add(TextsV2.GetStringNames(spare: EnumSpare.Antenna), GetFindText(TextsV2.FindAntenna, spare: EnumSpare.Antenna));
                }
                return textDictionary;
            case EnumQuest.PowerCell:
                if (QuestItemsFound() && !SpaceshipController.SpaceshipPartRepaired(State)) 
                {
                    textDictionary.Add(TextsV2.REPAIR_TEXT, TextsV2.RepairText);
                }
                else 
                {
                    textDictionary.Add(TextsV2.GetStringNames(spare: EnumSpare.PowerCell), GetFindText(TextsV2.FindPowerCell, spare: EnumSpare.PowerCell));
                    textDictionary.Add(TextsV2.GetStringNames(spare: EnumSpare.PowerCellCharge), GetFindText(TextsV2.FindPowerCellCharge, spare: EnumSpare.PowerCellCharge));
                }
                return textDictionary;
            default:
                return null;
        }
    }

    public bool TabletGreyText(string findText)
    {
        if (findText.Equals(TextsV2.GetStringNames(equipment: EnumEquipment.Flashlight)))
        {
            return InventoryController.EquipmentAvailable(EnumEquipment.Flashlight);
        }
        else if (findText.Equals(TextsV2.GetStringNames(equipment: EnumEquipment.Knife)))
        {
            return InventoryController.EquipmentAvailable(EnumEquipment.Knife);
        }
        else if (findText.Equals(TextsV2.GetStringNames(equipment: EnumEquipment.Wrench)))
        {
            return InventoryController.EquipmentAvailable(EnumEquipment.Wrench);
        }
        else if (findText.Equals(TextsV2.GetStringNames(spare: EnumSpare.Tape)))
        {
            return CurrentQuestItemFound(EnumSpare.Tape);
        }
        else if (findText.Equals(TextsV2.GetStringNames(spare: EnumSpare.Pipe)))
        {
            return CurrentQuestItemFound(EnumSpare.Pipe);
        }
        else if (findText.Equals(TextsV2.GetStringNames(spare: EnumSpare.Vinyl)))
        {
            return CurrentQuestItemFound(EnumSpare.Vinyl);
        }
        else if (findText.Equals(TextsV2.GetStringNames(spare: EnumSpare.Cable)))
        {
            return CurrentQuestItemFound(EnumSpare.Cable);
        }
        else if (findText.Equals(TextsV2.GetStringNames(spare: EnumSpare.Foil)))
        {
            return CurrentQuestItemFound(EnumSpare.Foil);
        }
        else if (findText.Equals(TextsV2.GetStringNames(spare: EnumSpare.Antenna)))
        {
            return CurrentQuestItemFound(EnumSpare.Antenna);
        }
        else if (findText.Equals(TextsV2.GetStringNames(spare: EnumSpare.PowerCell)))
        {
            return CurrentQuestItemFound(EnumSpare.PowerCell);
        }
        else if (findText.Equals(TextsV2.GetStringNames(spare: EnumSpare.PowerCellCharge)))
        {
            return CurrentQuestItemFound(EnumSpare.PowerCellCharge);
        }
        else
        {
            return false;
        }
    }

    private string GetFindText(string findText, EnumSpare spare = EnumSpare.Default, EnumEquipment equipment = EnumEquipment.Default) 
    {
        QuestStruct currentQuest = GetCurrentQuest();
        string equipmentAvailable = "1/1";
        string equipmentNeeded = "0/1";

        if (!equipment.Equals(EnumSpare.Default) && spare.Equals(EnumSpare.Default))
        {
            if (InventoryController.EquipmentAvailable(equipment))
            {
                return findText += equipmentAvailable;
            }
            else
            {
                return findText += equipmentNeeded;
            }
        }
        else if (!spare.Equals(EnumSpare.Default) && !currentQuest.ID.Equals(EnumQuest.Default) && equipment.Equals(EnumEquipment.Default))
        {
            int questAmount = currentQuest.GetQuestItemAmount(spare);
            int inventoryAmount = InventoryController.GetSpareAmount(spare);
            return findText += inventoryAmount + "/" + questAmount;
        }
        return "";
    }

    private void PlayQuestAudio(EnumQuest quest) 
    {
        switch (quest) 
        {
            case EnumQuest.Start:
                AudioManager.instance.AddAudioToAudioQueue("start");
                break;
            case EnumQuest.Knife:
                AudioManager.instance.AddAudioToAudioQueue("questKnife");
                break;
            case EnumQuest.Wrench:
                AudioManager.instance.AddAudioToAudioQueue("questWrench");
                break;
            case EnumQuest.Holes:
                AudioManager.instance.AddAudioToAudioQueue("questHoles");
                break;
            case EnumQuest.Leg:
                AudioManager.instance.AddAudioToAudioQueue("questLeg");
                break;
            case EnumQuest.Antenna:
                AudioManager.instance.AddAudioToAudioQueue("questAntenna");
                break;
            case EnumQuest.PowerCell:
                AudioManager.instance.AddAudioToAudioQueue("questPowerCell");
                break;
            case EnumQuest.End:
                AudioManager.instance.AddAudioToAudioQueue("end");
                break;
            case EnumQuest.Default:
                break;
            default:
                break;
        }

    }

    private bool CurrentQuestItemFound(EnumSpare spare) 
    {
        QuestStruct currentQuest = GetCurrentQuest();
        if (currentQuest.ID.Equals(EnumQuest.Default))
        {
            return false;
        }

        int inventoryAmount = InventoryController.GetSpareAmount(spare);
        int questAmount = currentQuest.GetQuestItemAmount(spare);
        if (inventoryAmount >= questAmount)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }

    private QuestStruct GetCurrentQuest()
    {
        foreach (QuestStruct quest in Quests)
        {
            if (quest.ID.Equals(State))
            {
                return quest;
            }
        }
        return new QuestStruct(null, EnumQuest.Default);
    }

    private bool QuestComplete() 
    {
        if (State.Equals(EnumQuest.Start))
        {
            return true;
        }
        else if (State.Equals(EnumQuest.End) || State.Equals(EnumQuest.Default))
        {
            return false;
        }
        else if (State.Equals(EnumQuest.Flashlight) || State.Equals(EnumQuest.Knife) || State.Equals(EnumQuest.Wrench))
        {
            return QuestItemsFound();
        }
        else 
        {
            return SpaceshipController.SpaceshipPartRepaired(State);
        }
    }

    private void InitQuestController() 
    {
        Dictionary<EnumSpare, int> quest = new Dictionary<EnumSpare, int>();
        //es muss 5 klebeband gefunden werden um die löcher zu flicken
        quest.Add(EnumSpare.Tape, 5);
        QuestStruct questHoles = new QuestStruct(quest, EnumQuest.Holes);
        //es muss 1 rohr, 1 venyl und 3 klebeband gefunden werden um das Bein zu reparieren
        quest = new Dictionary<EnumSpare, int>();
        quest.Add(EnumSpare.Pipe, 1);
        quest.Add(EnumSpare.Vinyl, 1);
        quest.Add(EnumSpare.Tape, 3);
        QuestStruct questLeg = new QuestStruct(quest, EnumQuest.Leg);
        //es muss 1 antenne, 5 kabel und 2 folie gefunden werden um die antenne zu reparieren
        quest = new Dictionary<EnumSpare, int>();
        quest.Add(EnumSpare.Antenna, 1);
        quest.Add(EnumSpare.Cable, 5);
        quest.Add(EnumSpare.Foil, 2);
        QuestStruct questAntenna = new QuestStruct(quest, EnumQuest.Antenna);
        //es muss 1 energiezelle gefunden werden
        //die energiezelle muss um 100 punkte aufgeladen werden
        quest = new Dictionary<EnumSpare, int>();
        quest.Add(EnumSpare.PowerCell, 1);
        quest.Add(EnumSpare.PowerCellCharge, 100);
        QuestStruct questPowerCell = new QuestStruct(quest, EnumQuest.PowerCell);

        Quests.Add(questHoles);
        Quests.Add(questLeg);
        Quests.Add(questAntenna);
        Quests.Add(questPowerCell);

        //init equipment
        Equipment.Add(EnumEquipment.Flashlight, false);
        Equipment.Add(EnumEquipment.Knife, false);
        Equipment.Add(EnumEquipment.Wrench, false);

        //questreihenfolge festlegen
        QuestOrder.Add(EnumQuest.Start);
        //QuestOrder.Add(EnumQuest.Flashlight);
        QuestOrder.Add(EnumQuest.Knife);
        QuestOrder.Add(EnumQuest.Holes);
        QuestOrder.Add(EnumQuest.Leg);
        QuestOrder.Add(EnumQuest.Wrench);
        QuestOrder.Add(EnumQuest.Antenna);
        QuestOrder.Add(EnumQuest.PowerCell);
        QuestOrder.Add(EnumQuest.End);

        State = EnumQuest.Start;
        InfoText = false;
        ItemsFoundState = EnumQuest.Default;
        PlayQuestAudio(State);
    }
}