﻿using System.Collections.Generic;

public struct QuestStruct
{
    private readonly Dictionary<EnumSpare, int> QuestItems;
    public EnumQuest ID { private set; get; }

    public QuestStruct(Dictionary<EnumSpare, int> items, EnumQuest id) 
    {
        if (items == null)
        {
            items = new Dictionary<EnumSpare, int>();
        }
        QuestItems = items;
        ID = id;
    }

    public int GetQuestItemAmount(EnumSpare spare)
    {
        if (QuestItems.ContainsKey(spare))
        {
            return QuestItems[spare];
        }
        return -1;
    }

    public IEnumerator<EnumSpare> GetQuestItems()
    {
        return QuestItems.Keys.GetEnumerator();
    }
}
