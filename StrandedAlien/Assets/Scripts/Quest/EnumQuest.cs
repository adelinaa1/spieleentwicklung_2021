﻿public enum EnumQuest
{
    Default,
    Start,
    Flashlight,
    Knife,
    Wrench,
    Holes,
    Leg,
    Antenna,
    PowerCell,
    End
}