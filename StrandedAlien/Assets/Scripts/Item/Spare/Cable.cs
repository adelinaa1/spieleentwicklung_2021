﻿public class Cable : Spare
{
    private readonly string infoText = TextsV2.CableInfoText;
    private readonly string errorText = TextsV2.CableErrorText;

    new void Start()
    {
        SpareType = EnumSpare.Cable;
        RequiredEquipment = EnumEquipment.Knife;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText);

        base.Start();
    }
}
