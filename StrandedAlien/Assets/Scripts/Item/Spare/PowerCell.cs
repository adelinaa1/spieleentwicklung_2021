﻿public class PowerCell : Spare
{
    private readonly string infoText = TextsV2.PowerCellInfoText;
    private readonly string errorText = TextsV2.PowerCellErrorText;

    new void Start()
    {
        SpareType = EnumSpare.PowerCell;
        RequiredEquipment = EnumEquipment.Default;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText);

        base.Start();
    }

}
