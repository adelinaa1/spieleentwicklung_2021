﻿public class Tape : Spare
{
    private readonly string infoText = TextsV2.TapeInfoText;
    private readonly string errorText = TextsV2.TapeErrorText;

    new void Start()
    {
        SpareType = EnumSpare.Tape;
        RequiredEquipment = EnumEquipment.Knife;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText);

        base.Start();
    }

}
