﻿public class Vinyl : Spare
{
    private readonly string infoText = TextsV2.VinylInfoText;
    private readonly string errorText = TextsV2.VinylErrorText;

    new void Start()
    {
        SpareType = EnumSpare.Vinyl;
        RequiredEquipment = EnumEquipment.Default;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText);

        base.Start();
    }
}
