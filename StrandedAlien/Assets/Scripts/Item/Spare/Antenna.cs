﻿public class Antenna : Spare
{
    private readonly string infoText = TextsV2.AntennaInfoText;
    private readonly string errorText = TextsV2.AntennaErrorText;

    new void Start()
    {
        SpareType = EnumSpare.Antenna;
        RequiredEquipment = EnumEquipment.Wrench;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText); 

        base.Start();
    }
}
