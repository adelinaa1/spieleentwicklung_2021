﻿public class PowerCellCharge : Spare
{
    private readonly string infoText = TextsV2.PowerCellChargeText;
    private readonly string errorText = TextsV2.PowerCellChargeErrorText;

    new void Start()
    {
        SpareType = EnumSpare.PowerCellCharge;
        RequiredEquipment = EnumEquipment.Default;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText);

        base.Start();
    }
}
