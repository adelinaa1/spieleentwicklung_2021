﻿public class Spare : Item
{
    public EnumSpare SpareType { get; set; }
    public EnumEquipment RequiredEquipment { get; set; }
}
