﻿public class Foil : Spare
{
    private readonly string infoText = TextsV2.FoilInfoText;
    private readonly string errorText = TextsV2.FoilErrorText;

    new void Start()
    {
        SpareType = EnumSpare.Foil;
        RequiredEquipment = EnumEquipment.Knife;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText);

        base.Start();
    }
}
