﻿public class Pipe : Spare
{
    private readonly string infoText = TextsV2.PipeInfoText;
    private readonly string errorText = TextsV2.PipeErrorText;

    new void Start()
    {
        SpareType = EnumSpare.Pipe;
        RequiredEquipment = EnumEquipment.Default;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText);

        base.Start();
    }

}
