﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour, IInteractable, ITargetable, IItem
{
    public GameObject Player;
    public GameObject ItemText;
    public Canvas UICanvas;

    private string ItemInfoText;
    private string ItemErrorText;
    private Text Text;
    private Image Background;
    private bool isTargetedOuter = false;
    private bool isTargetedInner = false;


    // Start is called before the first frame update
    public void Start()
    {
        Text = ItemText.GetComponentInChildren<Text>();
        Background = ItemText.GetComponentInChildren<Image>();
        SetItemTextVisibility(false);
    }

    // Update is called once per frame
    public void Update()
    {
        if (isTargetedInner != isTargetedOuter)
        {
            HighlightItem(isTargetedOuter);
            isTargetedInner = isTargetedOuter;
        }
    }

    public void HighlightItem(bool highlight)
    {
        if (!highlight)
        {
            SetItemTextVisibility(highlight);
            return;
        }
        if (this is Spare && !((Spare)this).RequiredEquipment.Equals(EnumEquipment.Default) &&
            !Player.GetComponent<InventoryController>().EquipmentAvailable(((Spare)this).RequiredEquipment))
        {
            Text.text = ItemInfoText + Environment.NewLine + ItemErrorText;
        }
        else
        {
            Text.text = ItemInfoText;
        }
        //set position
        //Transform crosshair = Player.transform.Find("Crosshair");
        Vector3 pos = Player.GetComponent<CharacterAndCameraController>().GetCrosshairPosition();
        float scale = UICanvas.scaleFactor;
        pos.x -= Text.preferredWidth * scale * 0.75f;
        Background.transform.position = pos;
        //show text
        SetItemTextVisibility(highlight);
    }

    public void Targeted(bool isTargeted_)
    {
        isTargetedOuter = isTargeted_;
    }

    public void Interaction()
    {

    }

    public void OnDisable()
    {
        HighlightItem(false);
    }

    public void SetItemInfoText(string text) 
    {
        ItemInfoText = text;
    }

    public void SetItemErrorText(string text) 
    {
        ItemErrorText = text;
    }

    private void SetItemTextVisibility(bool visible) 
    {
        Background.enabled = visible;
        Text.enabled = visible;
    }
}
