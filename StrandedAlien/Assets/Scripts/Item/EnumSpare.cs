﻿public enum EnumSpare 
{
    Default,
    //spares
    Tape,
    Cable,
    Foil,
    Pipe,
    //special spares
    PowerCell,
    PowerCellCharge,
    Antenna,
    Vinyl
}
