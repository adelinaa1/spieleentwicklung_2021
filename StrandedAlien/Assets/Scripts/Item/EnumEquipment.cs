﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnumEquipment
{
    Default,
    Flashlight,
    Knife,
    Wrench
}