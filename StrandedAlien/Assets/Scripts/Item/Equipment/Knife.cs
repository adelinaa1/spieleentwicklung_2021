﻿public class Knife : Equipment
{
    private readonly string infoText = TextsV2.KnifeInfoText;
    private readonly string errorText = TextsV2.KnifeErrorText;

    new void Start()
    {
        EquipmentType = EnumEquipment.Knife;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText);

        base.Start();
    }
}
