﻿public class Wrench : Equipment
{
    private readonly string infoText = TextsV2.WrenchInfoText;
    private readonly string errorText = TextsV2.WrenchErrorText;

    new void Start()
    {
        EquipmentType = EnumEquipment.Wrench;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText);

        base.Start();
    }
}
