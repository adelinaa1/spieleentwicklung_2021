﻿public class Flashlight : Equipment
{
    private readonly string infoText = TextsV2.FlashlightInfoText;
    private readonly string errorText = TextsV2.FlashlightErrorText;

    new void Start()
    {
        EquipmentType = EnumEquipment.Flashlight;

        base.SetItemInfoText(infoText);
        base.SetItemErrorText(errorText);

        base.Start();
    }
}
