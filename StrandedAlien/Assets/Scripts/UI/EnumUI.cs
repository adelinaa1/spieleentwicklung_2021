﻿public enum EnumUI
{
    Default,
    InfoTextQuestComplete,
    InfoTextQuestItemsFound,
    InfoTextRepairNotPossible,
    InfoTextNoRepair
}
