﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestController : MonoBehaviour
{
    public GameObject GameFlow;
    private QuestControllerV2 QuestController;
    private Transform Content;
    private Transform TaskText;

    private readonly float WIDTH_PADDING = 20;
    private readonly float HEIGHT_PADDING = 10;

    public void InitUIQuestController(Vector2 size) 
    {
        transform.GetComponent<RectTransform>().sizeDelta = size;
        QuestController = GameFlow.GetComponent<QuestControllerV2>();
        Content = transform.Find("Content");
        TaskText = transform.Find("Content/TaskText");
        Content.gameObject.SetActive(false);
    }

    public void UIQuestText()
    {
        /**
         * .activeInHierarchy gibt an ob das gameobject durch die hierarchie aktiv ist 
         * (der .setActive()-state wird an die kinder vererbt)
         * wenn die UI nicht angezeigt wird, muss kein text geladen werden
         */
        if (!gameObject.activeInHierarchy) 
        { 
            return;
        }

        /**
         * alle alten kind-textelemente entfernen
         */
        foreach (Transform child in Content)
        {
            if (!child.gameObject.name.Equals("MainText") && !child.gameObject.name.Equals("TaskText"))
            {
                Destroy(child.gameObject);
            }
        }

        Dictionary<string, string> texts = QuestController.GetQuestText();

        if (texts == null) 
        {
            Content.gameObject.SetActive(false);
            return;
        }
        
        if (texts.ContainsKey(TextsV2.REPAIR_TEXT))
        {
            string repairText = texts[TextsV2.REPAIR_TEXT];
            if (repairText.Equals(""))
            {
                texts.Remove(TextsV2.REPAIR_TEXT);
            }
            else if (repairText.Equals(TextsV2.RepairText))
            {
                //muss geändert werden wenn sich der text ändert
                //es kann dann nicht mehr nach dem und gefiltert werden sondern
                //nach dem wort an dem der zeilenumbruch eingefügt werden soll
                int index = repairText.IndexOf("und");
                repairText = repairText.Insert(index, "\n");
                texts.Clear();
                texts.Add(TextsV2.REPAIR_TEXT, repairText);
            }
        }
        float width = 0;
        float height = 0;

        if (texts.Count > 0)
        {
            Content.gameObject.SetActive(true);
            foreach (string key in texts.Keys)
            {
                Transform t = GameObject.Instantiate(TaskText, Content);
                t.gameObject.SetActive(true);
                t.GetComponent<Text>().text = texts[key];
                width = CalcAndSetWidth(t, width, height);
                height = CalcAndSetHeight(t, width, height);
                if (QuestController.TabletGreyText(key))
                {
                    t.GetComponent<Text>().color = Color.grey;
                }
            }
        }
        else 
        {
            Content.gameObject.SetActive(false);
        }
    }

    /**
     * berechnet die breite des "content"-Panels (blauer-transparenter bereich)
     * die breite ergibt sich aus dem kind-textelement das den längsten text enthält
     * also am breitesten ist
     */
    private float CalcAndSetWidth(Transform t, float width, float height) 
    {
        float tmpWidth = t.GetComponent<Text>().preferredWidth;
        if (tmpWidth > width)
        {
            width = tmpWidth;
        }
        t.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
        Content.GetComponent<RectTransform>().sizeDelta = new Vector2((width + WIDTH_PADDING), (height + HEIGHT_PADDING));
        return width;
    }

    /**
     * berechnet die höhe des "content"-Panels (blauer-transparenter bereich)
     * die höhe ergibt sich aus der summe der höhen aller kind-textelemente
     */
    private float CalcAndSetHeight(Transform t, float width, float height) 
    {
        float tmpHeight = t.GetComponent<Text>().preferredHeight;
        if (tmpHeight > height)
        {
            height = tmpHeight;
        }
        else
        {
            height += tmpHeight;
        }
        t.GetComponent<RectTransform>().sizeDelta = new Vector2(width, tmpHeight);
        Content.GetComponent<RectTransform>().sizeDelta = new Vector2((width + WIDTH_PADDING), (height + HEIGHT_PADDING));
        return height;
    }
}
