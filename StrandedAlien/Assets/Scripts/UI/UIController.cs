﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    //The GameFlow reference provides the following Controllers:
    //GameFlowController, which is needed for disable input, fade in, fade out and timers
    //QuestControllerV2, which is needed to update the quest state
    [SerializeField]
    private Transform GameFlow;
    private GameFlowController GameFlowController;
    private QuestControllerV2 QuestControllerV2;

    //The Spaceship reference provides the following Controller:
    //SpaceshipController, which is needed to call the rapair method when the overlay is displayed
    [SerializeField]
    private Transform Spaceship;
    private SpaceshipController SpaceshipController;

    //The Player reference is needed to display the info text depending on the crosshair position
    [SerializeField]
    private Transform Player;

    //reference to the game end image
    [SerializeField]
    private GameObject GameEndImage;
    
    //queue for ui elements to be displayed
    private LinkedList<KeyValuePair<EnumUI, int>> UIQueue = new LinkedList<KeyValuePair<EnumUI, int>>();
    //list of text to a ui element
    private Dictionary<EnumUI, string> InfoTexts = new Dictionary<EnumUI, string>();
    //reference to the infotext (ui element)
    private Transform InfoText;
    //timer for the Ui element
    private Timer UITimer;
    //flag that indicates whether or not a ui element is displayed
    private bool UIDisplayed = false;
    //reference to the currently displayed ui element
    private EnumUI CurrentUIElement;
    //holds the time the current ui element should be displayed
    private int UIElementDisplayDuration;

    //reference to the overlay
    private Transform Overlay;
    //timer for the overlay
    private Timer OverlayTimer;
    //flag that indicates whether or not the overlay should be displayed
    private bool DisplayOverlay = false;
    //flag that indiactes whether or not the overlay is displayed
    private bool OverlayDisplayed = false;
    //the duration the overlay is displayed
    [SerializeField]
    private float OverlayDuration;

    //reference to the ui canvas
    private Canvas UICanvas;

    //remove??????
    //could be done differently???
    private UIQuestController UIQuestController;

    void Start()
    {
        InfoText = transform.Find("InfoText");
        Overlay = transform.Find("Overlay/Background");
        UICanvas = transform.GetComponent<Canvas>();
        UIQuestController = transform.Find("QuestUI").GetComponent<UIQuestController>();
        SpaceshipController = Spaceship.GetComponent<SpaceshipController>();
        GameFlowController = GameFlow.GetComponent<GameFlowController>();
        QuestControllerV2 = GameFlow.GetComponent<QuestControllerV2>();

        UITimer = GameFlowController.GenerateTimer();
        OverlayTimer = GameFlowController.GenerateTimer();

        UIQuestController.InitUIQuestController(transform.GetComponent<RectTransform>().sizeDelta);

        Overlay.gameObject.SetActive(false);
        InfoText.gameObject.SetActive(false);

        InitOverlay();
    }

    void Update()
    {
        if (DisplayOverlay && OverlayDisplayed == false)
        {
            HideUI();
            StartCoroutine(GameFlowController.FadeInTextAndImageOfGameObject(Overlay.gameObject, SetOverlayDisplayedAndRepairShip));
            return;
        }
        if (OverlayDisplayed && OverlayTimer.GetTimerTime() >= OverlayDuration) 
        {
            StartCoroutine(GameFlowController.FadeOutTextAndImageOfGameObject(Overlay.gameObject, true, QuestControllerV2.UpdateQuestState));
            OverlayDisplayed = false;
            DisplayOverlay = false;
            return;
        }

        //if queue contains an ui element and neither another ui element nor the overlay is displayed or should be displayed, display first element in queue
        if (UIQueue.Count > 0 && UIDisplayed == false && DisplayOverlay == false && OverlayDisplayed == false) 
        {
            //get first element in ui queue
            KeyValuePair<EnumUI, int> tmpElement = UIQueue.First();
            
            InitInfoText(tmpElement.Key, InfoTexts[tmpElement.Key]);
            ShowUI(tmpElement.Key, tmpElement.Value);
            
            UIQueue.Remove(tmpElement);
            InfoTexts.Remove(tmpElement.Key);
        }
        if (UIDisplayed)
        {
            if (UITimer.GetTimerTime() >= UIElementDisplayDuration)
            {
                HideUI();
            }
            else 
            {
                UpdateUI();
            }
        }
    }

    public void DeactivateUIController() 
    {
        gameObject.SetActive(false);
    }

    public void ActivateUIController() 
    {
        gameObject.SetActive(true);
        UIQuestController.UIQuestText();
    }

    private void HideAllUIElements() 
    {
        foreach (Transform child in transform) 
        {
            if (child.gameObject.name.Equals("GameEndImage")) 
            {
                continue;
            }
            child.gameObject.SetActive(false);
        }
    }

    public void GameEnd() 
    {
        HideAllUIElements();
        StartCoroutine(GameFlowController.FadeInTextAndImageOfGameObject(GameEndImage.gameObject));
        StartCoroutine(GameEndWait());
    }

    private IEnumerator GameEndWait() 
    {
        yield return new WaitForSeconds(5);
        Cursor.visible = true;
        SceneManager.LoadSceneAsync("MainMenu", LoadSceneMode.Single);
    }

    public void ShowOverlay() 
    {
        DisplayOverlay = true;
    }

    private int SetOverlayDisplayedAndRepairShip() 
    {
        OverlayTimer.StartTimer();
        SpaceshipController.RepairSpaceship();
        OverlayDisplayed = true;
        return 0;
    }

    public void ShowUIElement(EnumUI uIType, int displayTime, string infoText = "") 
    {
        //fehler
        if (uIType.Equals(EnumUI.Default)) 
        {
            return;
        }
        //UI element ist ein info text
        else 
        {
            if (UIQueue.Contains(new KeyValuePair<EnumUI, int>(uIType, displayTime))) 
            {
                return;
            }
            UIQueue.AddLast(new KeyValuePair<EnumUI, int>(uIType, displayTime));
            InfoTexts.Add(uIType, infoText);
        }
    }

    private void InitInfoText(EnumUI textType, string infoText)
    {
        Text text = InfoText.GetComponentInChildren<Text>();
        //text festlegen
        text.text = infoText;
        //farbe des textes festlegen
        Color tmpColor = text.color;
        if (textType.Equals(EnumUI.InfoTextNoRepair) || textType.Equals(EnumUI.InfoTextRepairNotPossible))
        {
            tmpColor = Color.red;
        }
        else if (textType.Equals(EnumUI.InfoTextQuestItemsFound) || textType.Equals(EnumUI.InfoTextQuestComplete))
        {
            ColorUtility.TryParseHtmlString("#BF6E17", out tmpColor);
        }
        text.color = tmpColor;
    }

    private void SetInfoTextPositionRelativeToCrosshair()
    {
        Text text = InfoText.GetComponentInChildren<Text>();
        //get crosshair position
        Vector3 pos = Player.GetComponent<CharacterAndCameraController>().GetCrosshairPosition();

        //set info text position depending on crossahir position
        float scale = UICanvas.scaleFactor;
        pos.x += text.preferredWidth * scale * 0.75f;
        InfoText.transform.position = pos;
    }

    private int HideInfoText()
    {
        InfoText.gameObject.SetActive(false);
        //the fade out function sets the a-value of the color to 0
        //so it is necessary to set the value back to the one befor the fade out
        Color tmpColor = InfoText.GetComponentInChildren<Image>().color;
        tmpColor.a = 140;
        InfoText.GetComponentInChildren<Image>().color = tmpColor;
        ResetUI();
        return 0;
    }

    private void InitOverlay() 
    {
        Color tmpColor = Color.black;
        tmpColor.a = 0;
        Overlay.GetComponent<Image>().color = tmpColor;
        Overlay.gameObject.SetActive(false);
        if (OverlayDuration.Equals(0))
        {
            OverlayDuration = 0.7f;
        }
    }

    private int ResetUI() 
    {
        UITimer.StopTimer(true);
        UIDisplayed = false;
        return 0;
    }

    private void ShowUI(EnumUI uIType, int displayTime) 
    {
        UIElementDisplayDuration = displayTime;
        CurrentUIElement = uIType;
        UIDisplayed = true;
        UITimer.StartTimer();

        if (uIType.Equals(EnumUI.Default))
        {
            ResetUI();
            return;
        }
        else 
        {
            InfoText.gameObject.SetActive(true);
        }
    }

    private void HideUI() 
    {
        if (CurrentUIElement.Equals(EnumUI.Default))
        {
            //error case, do nothing
            return;
        }
        else 
        {
            StartCoroutine(GameFlowController.FadeOutTextAndImageOfGameObject(InfoText.gameObject, false, HideInfoText));
        }
    }

    private void UpdateUI() 
    {
        SetInfoTextPositionRelativeToCrosshair();
    }
}
