﻿using UnityEngine;
using UnityEngine.Rendering;
// using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;

public class DayNightController : MonoBehaviour
{
//     [SerializeField]
//     private GameObject Sun;
//     [SerializeField]
//     private GameObject Moon;
//     [SerializeField]
//     private GameObject BackgroundLight;
//     [SerializeField]
//     private float DayNightSpeed;
//     [SerializeField]
//     private GameObject PostProcessingGameObject;
//     [SerializeField]
//     private Slider SliderLightIntensity;
//     private LiftGammaGain LiftGammaGain;
//     //if the size of the terrain is changed this variable needs to be chnaged too
//     private Vector3 TerrainCenter = new Vector3(500f, 0f, 500f);
//     private bool isDay;
//     private float deg;
//     private float MaxSunIntensity;
//     private float MaxMoonIntensity;
//     private float sunLightChange;
//     private float moonLightChange;

//     private void Start()
//     {
//         isDay = true;
//         deg = 270f;
//         MaxSunIntensity = Sun.GetComponent<HDAdditionalLightData>().intensity;
//         MaxMoonIntensity = Moon.GetComponent<HDAdditionalLightData>().intensity;
//         sunLightChange = MaxSunIntensity / DayNightSpeed * 20;
//         moonLightChange = MaxMoonIntensity / DayNightSpeed * 20;
//         Moon.GetComponent<HDAdditionalLightData>().intensity = 0;

//         //following code is needed for gamme setting (slider)        
//         VolumeProfile profile = PostProcessingGameObject.GetComponent<Volume>().sharedProfile;
//         if (!profile.TryGet<LiftGammaGain>(out LiftGammaGain))
//         {
//             LiftGammaGain = profile.Add<LiftGammaGain>();
//         }
//         else 
//         {
//             ChangeGammaValue();
//         }
//         SliderLightIntensity.onValueChanged.AddListener(delegate { ChangeGammaValue(); });
//     }

//     private void DayNightCycle() 
//     {
//         /**
//          * if day night cycle is wanted add this method to the update function
//          */
//         Sun.transform.RotateAround(new Vector3(500f, 0f, 500f), Vector3.right, Time.deltaTime * DayNightSpeed);
//         Sun.transform.LookAt(TerrainCenter);

//         Moon.transform.RotateAround(new Vector3(500f, 0f, 500f), Vector3.right, Time.deltaTime * DayNightSpeed);
//         Moon.transform.LookAt(TerrainCenter);

//         deg += Time.deltaTime * DayNightSpeed;
//         if (Sun.transform.position.z >= 999.99)
//         {
//             deg = 0;
//         }

//         if (deg >= 170)
//         {
//             isDay = true;
//         }
//         if (deg >= 350)
//         {
//             isDay = false;
//         }

//         if (isDay)
//         {
//             Lighten(Sun, sunLightChange, MaxSunIntensity);
//             Darken(Moon, moonLightChange);
//         }
//         else
//         {
//             Lighten(Moon, moonLightChange, MaxMoonIntensity);
//             Darken(Sun, sunLightChange);
//         }
//     }

//     private void Lighten (GameObject gameObject, float speed, float maxIntensity)
//     {
//         HDAdditionalLightData hDAdditionalLightData = gameObject.GetComponent<HDAdditionalLightData>();
//         if (hDAdditionalLightData.intensity < maxIntensity)
//         {
//             hDAdditionalLightData.intensity += Time.deltaTime * speed;
//         }
//         if (hDAdditionalLightData.intensity > maxIntensity) 
//         {
//             hDAdditionalLightData.intensity = maxIntensity;
//         }
//     }

//     private void Darken(GameObject gameObject, float speed) 
//     {
//         HDAdditionalLightData hDAdditionalLightData = gameObject.GetComponent<HDAdditionalLightData>();
//         if (hDAdditionalLightData.intensity > 0)
//         {
//             hDAdditionalLightData.intensity -= Time.deltaTime * speed;
//         }
//     }

//     private void ChangeGammaValue()   
//     {
//         float value = SliderLightIntensity.value;
//         Debug.Log("slider value: " + value);
//         LiftGammaGain.gamma.Override(new Vector4(1f, 1f ,1f, value));
//     }
}
